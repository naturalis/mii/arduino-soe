/*
 * Zie assetlijst of topdesk
 *
 * INFOPUNTDINOTIJD [10.139.1.70]
 *
 */
#define IPSHOWCTL "10.139.1.1"
#define COMPONENT "infopuntdinotijd_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "INFOPUNTDINOTIJD_AAN"
#define UDPMSGOFF "INFOPUNTDINOTIJD_UIT"
