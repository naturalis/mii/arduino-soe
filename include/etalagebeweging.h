/*
 * Zie assetlijst of topdesk
 *
 * ETALAGEBEWEGING [10.130.1.70]
 *
 */
#define IPSHOWCTL "10.130.1.1"
#define COMPONENT "etalagebeweging_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "ETALAGEBEWEGING_AAN"
#define UDPMSGOFF "ETALAGEBEWEGING_UIT"
