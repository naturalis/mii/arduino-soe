/*
 * Zie assetlijst of topdesk
 *
 * INFOPUNTLEVEN [10.129.1.70]
 *
 */
#define IPSHOWCTL "10.129.1.1"
#define COMPONENT "infopuntleven_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "INFOPUNTLEVEN_AAN"
#define UDPMSGOFF "INFOPUNTLEVEN_UIT"
