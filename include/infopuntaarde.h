/*
 * Zie assetlijst of topdesk
 *
 * INFOPUNTAARDE [10.133.1.70]
 *
 */
#define IPSHOWCTL "10.133.1.1"
#define COMPONENT "infopuntaarde_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "INFOPUNTAARDE_AAN"
#define UDPMSGOFF "INFOPUNTAARDE_UIT"
