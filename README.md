# Switch Over Ethernet (SOE)

This project contains the software for creating a ESP32 network switch for sending INFOPUNT_AAN or
INFOPUNT_UIT over UDP.

Based on the arduino-esp32
examples [ETH_LAN8720 and WiFiUDPClient from espressif](https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi/examples).

And Arduino's State Change Detection (Edge Detection) for pushbuttons
[example code](https://www.arduino.cc/en/Tutorial/StateChangeDetection).

To build and push the code to a specific SOE use the environment variable:

```bash
pio run -e infopuntaarde 
```

Check `platformio.ini` to see all environments. Verify the ip and the details
in the include files.
